const mysql = require("mysql2");

const connection = mysql.createPool({
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
});

connection.getConnection((err) => {
  if (err instanceof Error) {
    console.log("getconnection err:", err);
    return;
  }
});
module.exports = connection;
