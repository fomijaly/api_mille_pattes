const express = require("express");
const router = express.Router();

const connection = require("../config/db");

router.get(`/`, (req, res) => {
  connection.query(`SELECT * FROM animal_owner`, (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de l'affichage");
    } else {
      res.json(results);
    }
  });
});

router.get(`/:id`, (req, res) => {
  const id = req.params.id;
  connection.execute(
    `SELECT * FROM animal_owner WHERE animal_owner_id = ?`,
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.json(results);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM animal_owner WHERE animal_owner_id = ? ",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.send(`Le propriétaire a été supprimé`);
      }
    }
  );
});

module.exports = router;