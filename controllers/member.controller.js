const express = require("express");
const router = express.Router();

const connection = require("../config/db");

router.get(`/`, (req, res) => {
  connection.query(`SELECT * FROM member`, (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de l'affichage");
    } else {
      res.json(results);
    }
  });
});

router.get(`/:id`, (req, res) => {
  const id = req.params.id;
  connection.execute(
    `SELECT * FROM member WHERE member_id = ?`,
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.json(results);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM member WHERE member_id = ? ",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.send(`Le membre a été supprimé`);
      }
    }
  );
});

module.exports = router;