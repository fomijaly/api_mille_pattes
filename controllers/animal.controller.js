const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");

const connection = require("../config/db");
router.use(express.static('../public'))

const storage = multer.diskStorage({
  destination: (req, res, db) => {
    db(null, path.join('/Users/apprenant12/front_mille_pattes/public/images'));
  },
  filename: (req, file, db) => {
    db(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const upload = multer({
  storage: storage,
});

router.get(`/`, (req, res) => {
  connection.query(
    `
  SELECT * FROM animal a 
  INNER JOIN animal_owner ao 
  ON a.animal_owner_id = ao.animal_owner_id `,
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'affichage");
      } else {
        res.json(results);
      }
    }
  );
});

router.get(`/:id`, (req, res) => {
  const id = req.params.id;
  connection.execute(
    `SELECT * FROM animal WHERE animal_id = ?`,
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.json(results);
      }
    }
  );
});

router.post("/", upload.single('animal_picture'), (req, res) => {
  const { name, birthday, specie, description, animal_owner_id } =
    req.body;
  const animal_picture = req.file.filename;

  connection.execute(
    "INSERT INTO animal (name, birthday, specie, description, animal_owner_id, animal_picture) VALUES (?, ?, ?, ?, ?, ?)",
    [name, birthday, specie, description, animal_owner_id, animal_picture],
    (err, results) => {
      if (err) {
        console.error("Erreur lors de l'insertion de l'animal:", err);
        res.status(500).send({ error: err });
      } else {
        console.log("L'animal a été rajouté");
        res.send(`L'animal a été rajouté`);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM animal WHERE animal_id = ? ",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.send(`L'animal a été supprimé`);
      }
    }
  );
});

module.exports = router;
