const express = require("express");
const router = express.Router();

const connection = require("../config/db");

router.get(`/`, (req, res) => {
  connection.query(`SELECT * FROM medicine`, (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de l'affichage");
    } else {
      res.json(results);
    }
  });
});

router.get(`/:id`, (req, res) => {
  const id = req.params.id;
  connection.execute(
    `SELECT * FROM medicine WHERE medicine_id = ?`,
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.json(results);
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM medicine WHERE medicine_id = ? ",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        res.send(`Le médicament a été supprimé`);
      }
    }
  );
});

module.exports = router;