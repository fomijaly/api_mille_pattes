require("dotenv").config();
const express = require("express");

const cors = require("cors");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(
  cors({
    origin: process.env.FRONTEND_URL,
  })
);

const port = 3000;

const animalControllers = require("./controllers/animal.controller");
app.use("/animals", animalControllers);

const animalOwnerControllers = require("./controllers/animalOwner.controller");
app.use("/animal_owners", animalOwnerControllers);

const medicineControllers = require("./controllers/medicine.controller");
app.use("/medicines", medicineControllers);

const memberControllers = require("./controllers/member.controller");
app.use("/members", memberControllers);

app.listen(port, () => {
  console.log(`Le serveur est écouté sur le port ${port}`);
});
